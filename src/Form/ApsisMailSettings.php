<?php

namespace Drupal\apsis_mail\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\StateInterface;
use Drupal\apsis_mail\Apsis;
use Drupal\apsis_mail\Exception\ApsisException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form.
 */
class ApsisMailSettings extends ConfigFormBase {

  use MessengerTrait;

  const CACHE_LIFETIME_OPTIONS = [
    30 => '30 sec',
    43200 => '12 hours',
  ];

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Apsis service.
   *
   * @var \Drupal\apsis_mail\Apsis
   */
  protected $apsis;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, StateInterface $state, Apsis $apsis) {
    parent::__construct($configFactory);
    $this->state = $state;
    $this->apsis = $apsis;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('apsis')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apsis_mail_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'apsis_mail.admin',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get config and states.
    $config = $this->config('apsis_mail.admin');
    $api_key = $this->state->get('apsis_mail.api_key');
    $mailing_lists = $this->state->get('apsis_mail.mailing_lists');
    $demographic_data = $this->state->get('apsis_mail.demographic_data');
    $always_show_demographic_data = $this->state->get('apsis_mail.demographic_data.always_show');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API'),
    ];

    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => $this->t('API key goes here.'),
      '#default_value' => $api_key,
    ];

    $form['api']['endpoint'] = [
      '#type' => 'details',
      '#title' => $this->t('Endpoint'),
      '#open' => FALSE,
    ];

    $form['api']['endpoint']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#description' => $this->t('URL to API method.'),
      '#default_value' => $config->get('api_url'),
    ];

    $form['api']['endpoint']['api_ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use SSL'),
      '#description' => $this->t('Use secure connection.'),
      '#default_value' => $config->get('api_ssl'),
    ];

    $form['api']['endpoint']['api_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSL port'),
      '#description' => $this->t('API endpoint SSL port number.'),
      '#default_value' => $config->get('api_port'),
      '#states' => [
        'visible' => [
          ':input[name="api_ssl"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['api']['endpoint']['cache_lifetime'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache lifetime'),
      '#description' => $this->t('Cache the result of requests for this long.'),
      '#options' => self::CACHE_LIFETIME_OPTIONS,
      '#default_value' => $config->get('cache_lifetime'),
    ];

    $form['optin'] = [
      '#type' => 'details',
      '#title' => $this->t('Double opt-in settings'),
      '#description' => $this->t(
        'With double opt-in users are sent a mail with a confirmation link when subscribing.'
      ),
    ];

    $form['optin']['optin_background'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable for non-profile page forms'),
      '#default_value' => $config->get('optin_background'),
    ];

    // Get user roles.
    $user_roles = user_roles(TRUE);
    $roles = [];
    foreach ($user_roles as $role) {
      $roles[$role->id()] = $role->label();
    }

    $form['user_roles'] = [
      '#type' => 'details',
      '#title' => $this->t('Control subscriptions on user profile'),
      '#description' => $this->t(
        'Enables users with corresponding role(s) selected to subscribe
        and unsubscribe to mailing lists via their user edit page.'
      ),
    ];

    $form['user_roles']['user_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $roles,
      '#default_value' => $config->get('user_roles') ? $config->get('user_roles') : [],
    ];

    $form['gdpr'] = [
      '#type' => 'details',
      '#title' => $this->t('GDPR settings'),
      '#description' => $this->t(
        'Enable the possibility to show a checkbox at the end of the form
        to accept the GDPR conditions.'
      ),
    ];

    $form['gdpr']['gdpr_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable checkbox to accept GDPR conditions.'),
      '#name' => 'gdpr_enabled',
      '#default_value' => $config->get('gdpr_enabled'),
    ];

    $form['gdpr']['gdpr_title'] = [
      '#type' => 'textarea',
      '#title' => $this->t('GDPR title.'),
      '#description' => $this->t('The text that you enter here will be shown in the title of the checkbox.'),
      '#default_value' => $config->get('gdpr_title'),
      '#states' => [
        'visible' => [
          ':input[name="gdpr_enabled"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="gdpr_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    try {
      $apsis_mailing_lists = $this->apsis->getMailingLists();
      $form['mailing_lists'] = [
        '#type' => 'details',
        '#title' => $this->t('Mailing lists'),
        '#description' => $this->t('Globally allowed mailing lists on site'),
      ];

      $form['mailing_lists']['mailing_lists'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed mailing lists'),
        '#options' => $apsis_mailing_lists,
        '#default_value' => $mailing_lists ? $mailing_lists : [],
      ];
    }
    catch (ApsisException $e) {
      // Do nothing.
    }

    try {
      $apsis_demographic_data = $this->apsis->getDemographicData();
      $form['demographic_data'] = [
        '#type' => 'details',
        '#title' => $this->t('Demographic data'),
        '#description' => $this->t('Globally allowed demographic data on site'),
      ];

      $form['demographic_data']['always_show'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Always show'),
        '#description' => $this->t('This will enforce all blocks to show demographic data'),
        '#default_value' => $always_show_demographic_data,
      ];

      $form['demographic_data']['demographic_data'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('APSIS Parameter'),
          $this->t('Label on block'),
          $this->t('Available on block'),
          $this->t('Required'),
          $this->t('Checkbox'),
          $this->t('Value when checkbox is checked'),
        ],
      ];

      foreach ($apsis_demographic_data as $key => $demographic) {
        $alternatives = $demographic['alternatives'];

        $form['demographic_data']['demographic_data'][$key]['key'] = [
          '#plain_text' => $key,
        ];

        $form['demographic_data']['demographic_data'][$key]['label'] = [
          '#type' => 'textfield',
          '#default_value' => !empty($demographic_data[$key]) ? $demographic_data[$key]['label'] : '',
        ];

        $form['demographic_data']['demographic_data'][$key]['available'] = [
          '#type' => 'checkbox',
          '#default_value' => !empty($demographic_data[$key]) ? $demographic_data[$key]['available'] : '',
        ];

        $form['demographic_data']['demographic_data'][$key]['required'] = [
          '#type' => 'checkbox',
          '#default_value' => !empty($demographic_data[$key]) ? $demographic_data[$key]['required'] : '',
          '#disabled' => (count($alternatives) > 2 || !$alternatives) ? FALSE : TRUE,
        ];

        $form['demographic_data']['demographic_data'][$key]['checkbox'] = [
          '#type' => 'checkbox',
          '#default_value' => !empty($demographic_data[$key]) ? $demographic_data[$key]['checkbox'] : '',
          '#disabled' => (count($alternatives) == 2) ? FALSE : TRUE,
        ];

        $form['demographic_data']['demographic_data'][$key]['return_value'] = [
          '#type' => (count($alternatives) == 2) ? 'select' : NULL,
          '#options' => (count($alternatives) == 2) ? $alternatives : NULL,
          '#default_value' => !empty($demographic_data[$key]['return_value']) ? $demographic_data[$key]['return_value'] : '',
        ];
      }
    }
    catch (ApsisException $e) {
      // Do nothing.
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save states.
    $this->state->setMultiple([
      'apsis_mail.api_key' => $form_state->getValue('api_key') ? $form_state->getValue('api_key') : '',
      'apsis_mail.mailing_lists' => $form_state->getValue('mailing_lists') ? array_filter($form_state->getValue('mailing_lists')) : [],
      'apsis_mail.demographic_data' => $form_state->getValue('demographic_data') ? array_filter($form_state->getValue('demographic_data')) : [],
      'apsis_mail.demographic_data.always_show' => $form_state->getValue('always_show'),
    ]);

    // Save settings.
    $this->config('apsis_mail.admin')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_ssl', $form_state->getValue('api_ssl'))
      ->set('api_port', $form_state->getValue('api_port'))
      ->set('optin_background', $form_state->getValue('optin_background'))
      ->set('user_roles', $form_state->getValue('user_roles'))
      ->set('gdpr_enabled', $form_state->getValue('gdpr_enabled'))
      ->set('gdpr_title', $form_state->getValue('gdpr_title'))
      ->set('cache_lifetime', intval($form_state->getValue('cache_lifetime')))
      ->save();

    $this->messenger()->addStatus($this->t('Settings saved.'));
  }

}
