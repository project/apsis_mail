<?php

namespace Drupal\apsis_mail\Exception;

/**
 * Exception thrown in case of missing configuration.
 */
class ConfigurationError extends \RuntimeException {

}
