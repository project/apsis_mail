<?php

namespace Drupal\apsis_mail\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\apsis_mail\Apsis;
use Drupal\apsis_mail\Exception\ApiDisabledException;
use Drupal\apsis_mail\Exception\BusyException;
use Drupal\apsis_mail\Exception\ConfigurationError;
use Drupal\apsis_mail\Exception\OptOutSubscriberException;
use Drupal\apsis_mail\Exception\UnauthorizedException;
use Drupal\apsis_mail\Exception\ValidationErrorException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Add subscribers to Apsis.
 *
 * @QueueWorker(
 *   id = "apsis_mail_add_subscriber",
 *   title = @Translation("Add subscribers to Apsis"),
 *   cron = {"time" = 60}
 * )
 */
class AddSubscriber extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  use LoggerAwareTrait;

  /**
   * The Apsis instance.
   *
   * @var \Drupal\apsis_mail\Apsis
   */
  protected $apsis;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  public $config;

  /**
   * Create new AddSubscriber queue worker.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerInterface $logger,
    Apsis $apsis,
    ImmutableConfig $config,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->apsis = $apsis;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $loggerFactory = $container->get('logger.factory');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $loggerFactory->get('apsis_mail'),
      $container->get('apsis'),
      $container->get('apsis.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    [$email, $list_id, $name, $demographic_data] = $data;
    try {
      if ($this->config->get('optin_background')) {
        $this->apsis->addOptInSubscriber($email, $list_id, $name, $demographic_data);;
      }
      else {
        $this->apsis->addSubscriber($email, $list_id, $name, $demographic_data);;
      }
    }
    catch (ConfigurationError $e) {
      throw new SuspendQueueException($e);
    }
    catch (OptOutSubscriberException $e) {
      // User on opt-out list. Log for good measure and move on.
      $this->logger->notice($e->getMessage());
    }
    catch (ValidationErrorException $e) {
      // Invalid data. Nothing really we can do here. Log and more on.
      $this->logger->error(sprintf('Unable to subscribe email %s to list %s: %s', $email, $list_id, $e->getMessage()));
    }
    catch (ApiDisabledException $e) {
      throw new SuspendQueueException($e);
    }
    catch (BusyException $e) {
      throw new SuspendQueueException($e);
    }
    catch (UnauthorizedException $e) {
      throw new SuspendQueueException($e);
    }
  }

}
