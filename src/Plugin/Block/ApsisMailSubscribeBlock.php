<?php

namespace Drupal\apsis_mail\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\apsis_mail\Apsis;
use Drupal\apsis_mail\Exception\ApsisException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a simple block.
 *
 * @Block(
 *   id = "apsis_mail_subscribe_block",
 *   admin_label = @Translation("Apsis mail subscribe block")
 * )
 */
class ApsisMailSubscribeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Apsis service.
   *
   * @var \Drupal\apsis_mail\Apsis
   */
  protected $apsis;

  /**
   * Form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $configFactory,
    Apsis $apsis,
    FormBuilderInterface $formBuilder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->apsis = $apsis;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('apsis'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->configuration;

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body text'),
      '#default_value' => !empty($config['body']) ? $config['body']['value'] : '',
    ];

    // Get allowed mailing lists.
    try {
      $mailing_lists = $this->apsis->getAllowedMailingLists();
      $demographic_data = $this->apsis->getAllowedDemographicData();
    }
    catch (ApsisException $e) {
      $mailing_lists = [];
      $demographic_data = [];
    }

    // Display a link to the admin configuration,
    // if there is no allowed mailing lists.
    if (empty($mailing_lists)) {
      // Get admin link.
      $url = Url::fromRoute('apsis_mail.admin');
      $link = Link::fromTextAndUrl($this->t('admin page'), $url);
      // Set no lists message.
      $message = $this->t('No allowed mailing lists are set, go to the @link to configure.', ['@link' => $link]);
      $this->messenger()->addError($message);
    }
    else {
      // Add 'exposed' option.
      $exposed = ['exposed' => $this->t('Let user choose')];
      $mailing_lists = $exposed + $mailing_lists;

      $form['mailing_list'] = [
        '#type' => 'select',
        '#title' => $this->t('Mailing list'),
        '#description' => $this->t('Mailing list to use'),
        '#options' => $mailing_lists,
        '#default_value' => !empty($config['mailing_list']) ? $config['mailing_list'] : 'exposed',
        '#required' => TRUE,
      ];

      if (!empty($demographic_data)) {
        $form['demographic_data'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Show demographic data'),
          '#description' => $this->t('Expose demographic data fields to user.'),
          '#default_value' => !empty($config['demographic_data']) ? $config['demographic_data'] : '',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('body', $form_state->getValue('body'));
    $this->setConfigurationValue('mailing_list', $form_state->getValue('mailing_list'));
    $this->setConfigurationValue('demographic_data', $form_state->getValue('demographic_data'));
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configuration;

    if (empty($config['mailing_list'])) {
      // Set warning and return empty array if no list is selected.
      $msg = $this->t('@prefix No newsletter selected', ['@prefix' => 'Apsis mail block: ']);
      $this->messenger()->addWarning($msg);
      return [];
    }

    // Get form.
    $form = $this->formBuilder->getForm('Drupal\apsis_mail\Form\SubscribeForm', $config['mailing_list'], $config['demographic_data']);

    // Put body content into a render array.
    $body = [
      '#markup' => $config['body']['value'],
    ];

    $output = [
      '#theme' => 'apsis_mail_block',
      '#body' => $body,
      '#form' => $form,
    ];

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(),
      $this->configFactory->get('system.site')->getCacheTags()
    );
  }

}
